#include <iostream>
#include <fstream>
#include <math.h>
#include "konstanten.h"
using namespace std;

// Berechnung von J(t) nach der gegebenen Formel
double Strahlung (double Tp, double lambda, double durchmesser)
{
	double g = (h * c0) / (lambda * b * Tg);
	double realteil = (-1) * n * 8 * pow(M_PI, 2) * h * pow(c0, 2) * 1 / ((pow(R, 2) * pow( lambda, 6)));
	double imaginaer = ((pow(m,2) - complex<double>(1,0)) / (pow(m,2) + complex<double>(2,0))).imag();	
	return realteil * imaginaer * pow(durchmesser, 3) * 1 / (exp( g * Tg / Tp) - 1 ) - pow(durchmesser, 3) * 1 / (exp(g) - 1);
}

//Berechnung des Signalwerts für jeden Temperaturwert durch wiederholtes AUfrufen der Funktion Strahlung
double** StrahlungskurveBerechnen (int laenge, double** temperaturKurve, double lambda, double durchmesser )
{
	double** Strahlungskurve= new double*[laenge];
	for(int i = 0; i < laenge; i++){
		Strahlungskurve[i] = new double[2];
		Strahlungskurve[i][0] = temperaturKurve[i][0]; //einlesen der Zeiten
		double temperatur = temperaturKurve[i][1]; //Zwischenspeichern der Temperatur zum Übergeben der Funktion Strahlung
		Strahlungskurve[i][1] = Strahlung(temperatur,lambda,durchmesser); //einlesen der Signalwerte aus unserer Funktion Strahlung
	}
	return Strahlungskurve;
}