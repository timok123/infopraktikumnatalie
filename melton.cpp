#include <cmath>
#include "konstanten.h"
#include <iostream>

//Verfahren nach Melton aus Aufgabenstellung
double melton(double Tp, double dp) 
{
	double mp = exp(log(roh) + log(4.0/3) +log(M_PI) + log(pow(dp / 2, 3)));
	double Qdot = exp( log(2) + log(k_g) + log(M_PI) + log(dp * dp) + log(Tp - Tg) - log(dp + G * lambda_mg));
	return (-1) * exp( log(Qdot) - log(mp * c_p_Fe));	
}

// Lösen der DGL nach dem Runge Kutta Verfahren
double** runge(double h, int laenge, double d, double T0)
{
	double** kurve = new double*[laenge]; // Kurve für das Speichern der Temperaturen
	kurve[0] = new double[2];
	kurve[0][0] = 0; // Zeitpunkt 0
	kurve[0][1] = T0; //Temperaturmaximum wird als Starttemperatur festgelegt
	for (int i = 0; i < laenge - 1; i++) {
		
		double k1 = h * melton(kurve[i][1], d);
		double k2 = h * melton(kurve[i][1] + 0.5 * k1, d + 0.5 * h);
		double k3 = h * melton(kurve[i][1] + 0.5 * k2, d + 0.5 * h);
		double k4 = h * melton(kurve[i][1] + k3, d + h);
		
		kurve[i + 1] = new double[2];
		kurve[i + 1][0] = kurve[i][0] + h; // reinschreiben der Zeitschritte
		kurve[i + 1][1] = kurve[i][1] + (1.0/6.0) * (k1 + k2 * 2 + k3 * 2 + k4); // reinschreiben der berechneten Temperatur
	}
	return kurve;
}