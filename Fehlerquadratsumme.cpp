#include <iostream>
#include <fstream>
#include <math.h>
#include "konstanten.h"
using namespace std;


double Fehlerquadratsumme(double** messung, double** rechnung, int zeilen){
	double Fehler = 0.0;
	for(int i = 0;i < zeilen; i++){
		double sub = rechnung[i][1] - messung[i][1];
		Fehler += pow(sub,2);
	}
	return Fehler / zeilen;
}

//Normierung
double** normierung(double** matrix, int laenge)
{
	double max = matrix[0][1];
	double** normierteMatrix = new double*[laenge];
	for (int i = 0; i < laenge; i++){
		normierteMatrix[i] = new double [2];
		normierteMatrix[i][0] = matrix[i][0]; // für Zeit
		normierteMatrix[i][1] = matrix[i][1] / max; //für Werte
	}
	return normierteMatrix;
}

//1. Abkühlkurve berechnen, 2. Strahlungssignal modellieren, 3. normieren
double** berechneSignalkurve( double dp, double T0, int laenge, double zeitschritt, double lambda){
	double** Abkuehlkurve = runge(zeitschritt, laenge, dp, T0); //Runge Kutta Verfahren
	double** Strahlungskurve = new double* [laenge];
	Strahlungskurve = StrahlungskurveBerechnen (laenge, Abkuehlkurve, lambda, dp);
	return normierung(Strahlungskurve, laenge);
}

double Bisektionsverfahren (int laenge, double** messung1, double** messung2, double T0){
	double minDurchmesser = 10e-9; //Bereich in dem der Durchmesser vermutet wird
	double maxDurchmesser = 1e-6;
	double zeitschritt = messung1[1][0] - messung1[0][0];
	double minFehler, maxFehler;
	do {
		//Signal berechnen aus minDurchmesser
		double** min1 = berechneSignalkurve(minDurchmesser, T0, laenge, zeitschritt, lambda1); //für 450nm
		double** min2 = berechneSignalkurve(minDurchmesser, T0, laenge, zeitschritt, lambda2); // für 650nm
		minFehler = Fehlerquadratsumme(messung1, min1, laenge) + Fehlerquadratsumme(messung2, min2, laenge);
		
		//Signal berechnen aus maxDurchmesser
		double** max1 = berechneSignalkurve(maxDurchmesser, T0, laenge, zeitschritt, lambda1); //für 450nm
		double** max2 = berechneSignalkurve(maxDurchmesser, T0, laenge, zeitschritt, lambda2); // für 650nm
		maxFehler = Fehlerquadratsumme(messung1, max1, laenge) + Fehlerquadratsumme(messung2, max2, laenge);
		
		if(minFehler < maxFehler)
		{ 
			maxDurchmesser = (minDurchmesser + maxDurchmesser) / 2;
		}
		
		if(maxFehler < minFehler)
		{
			minDurchmesser = (minDurchmesser + maxDurchmesser) / 2;			
		}
	
	} while(abs(maxFehler - minFehler) > 5e-5); //Abbruchbedingung; auf 1 nm genau
	return (minDurchmesser + maxDurchmesser) / 2;	
}
