#include <iostream>
#include <fstream>
#include <math.h>
#include "konstanten.h"
#include "Strahlung.cpp"
#include "melton.cpp"
#include "Fehlerquadratsumme.cpp"

using namespace std;

int groesse(const char* data){ //Länge der Datei
	int anzahl = 0;
	ifstream file_(data);
	file_.ignore(256, '\n'); //ignoriere erste Zeile
	double sek;
	double signal;
	if(file_.is_open())
	{
		while(file_ >> sek >> signal)
		{
			anzahl++;
		}
		file_.close();
	}
	return anzahl;
}

double** lesen(const char* data, int zeilenzahl) {// allokiert Speicher und liest die Datei ein
	ifstream file_ (data);
	double** werte = new double*[zeilenzahl];
	file_.ignore(256, '\n'); //ignoriere erste Zeile
	double sek;
	double signal;
	int i = 0;
	if(file_.is_open())
	{
		while(file_ >> sek >> signal)
		{
			werte[i] = new double[2];
			werte[i][0] = sek;
			werte[i][1] = signal;
			i++;
		}
		file_.close();
	}
	return werte;
}

void loeschen(double** A, int g) { //Speicher freigeben
    for (int i = 0; i < g; i++) {
        delete[] A[i];
    }
    delete[] A;

}

int maxsignalz(int zeilen,double** B){ //Bestimme Zeile vom Maximalwert
	double max = 0.0;   
    int pos = 0;      
   	for (int i = 0; i < zeilen; i++) {  
       	if (B[i][1] > max) {
           	max = B[i][1];
           	pos = i;
       	}
   	}
    return pos;
}

double** synchronisation(int signalz, int zeilen, double** liste) { //Synchronisieren
    double** werte = new double* [zeilen - signalz]; 
	    for (int i = 0; i < zeilen - signalz; i++) {
        werte[i] = new double[2];
    }
    for (int i = signalz, j = 0; i < zeilen; i++, j++) {
		    werte[j][0] = liste[j][0]; //reinschreiben der Zeit
	        werte[j][1] = liste[i][1]; //reinschreiben der Werte ab dem Maximum
		    }
    return werte;
}

double Tmax(double w450, double w650){ //maximale Temperatur bestimmen aus unseren maximalen Signalwerten (w450 = Temperaturmaximum für Signal 450nm)
	double T;
	T = exp(log(h)+log(c0)-log(b))*(exp(-log(lambda2))-exp(-log(lambda1)))/(log(w450)+ 6.0*log(lambda1)-log(w650)-6.0*log(lambda2));
	return T;
}

int main(){
	double smax_450, smax_650, T;
	int z_450, z_650, zmax_450, zmax_650, synz_450, synz_650, Messreihe; 
	cout << "Welche Messreihe?" << endl;
	cin >> Messreihe;
	if(Messreihe != 1 && Messreihe != 2 && Messreihe != 3){
		cout<<"Diese Messreihe existiert nicht."<<endl;
		return 0;
	}
	
	if(Messreihe == 1){
		z_450 = groesse("C:\\BP1_450nm.txt"); //Länge bestimmen für Array
    	double** eBP_450 = lesen("C:\\BP1_450nm.txt", z_450); //Signalwerte 450 einlesen
		z_650 = groesse("C:\\BP1_650nm.txt");
    	double** eBP_650 = lesen("C:\\BP1_650nm.txt", z_650);// Signalwerte 650 einlesen
    	
    	zmax_450 = maxsignalz(z_450,eBP_450); //Zeile beim Startwert
    	zmax_650 = maxsignalz(z_650,eBP_650);
		smax_450 = eBP_450[zmax_450][1]; //maximales Signal (Startwert)
		smax_650 = eBP_650[zmax_650][1];
		
		int laenge = min(z_450 - zmax_450, z_650 - zmax_650); //Ermitteln der kürzeren Länge der Arrays 
    	double** syn_450 = new double* [laenge]; //synchronisieren
    	syn_450 = synchronisation(zmax_450, z_450, eBP_450);
    	double** syn_650 = new double* [laenge];
    	syn_650 = synchronisation(zmax_650, z_650, eBP_650);
    	
		T = Tmax(smax_450, smax_650);
    	cout << "Die maximale Temperatur ist " << T << endl;
		
		double zeitschritt = syn_450[1][0] - syn_450[0][0]; // Berechnen des Zeitschrittes für Runge Kutta Verfahren

		//Normierung der eingelesenen Messdaten
		double** normierte_450 = new double* [laenge];
		normierte_450 = normierung (syn_450, laenge);
		double** normierte_650 = normierung(syn_650, laenge);
		normierte_650 = normierung(syn_650, laenge);
				
		long double d = 0; // wahrer Partikeldurchmesser
		long double dmin = 1e-8; //  Bereich in dem der Durchmesser erwartet wird
		long double dmax = 1e-6;
		long double e_2 = 0; //Fehlerquadratsumme
		long double e_3 = 1e20; // gewünschte Genauigkeit
		long double j = 0; //Anzahl der Werte
		for(long double dp = dmin;dp <= dmax; dp += 2e-9){	
			double** vergleichsmatrix_450 = new double* [laenge];
			vergleichsmatrix_450 = berechneSignalkurve(dp,T,laenge,1e-8,lambda1);
			double** vergleichsmatrix_650 = new double* [laenge];
			vergleichsmatrix_650 = berechneSignalkurve(dp,T,laenge,1e-8,lambda2);
			for (int i = 0; i < laenge; i++) { //Berechnung der Fehlerquadratsumme für 450 nm
				e_2 += (vergleichsmatrix_450[i][1] - normierte_450[i][1]) * (vergleichsmatrix_450[i][1] - normierte_450[i][1]);
				j++;
				//cout << e_2 << endl;
			}
			for (int i = 0; i < laenge; i++) { //Berechnung der Fehlerquadratsumme für 650nm
				e_2 += (vergleichsmatrix_650[i][1] - normierte_650[i][1]) * (vergleichsmatrix_650[i][1] - normierte_650[i][1]);
				j++;
				//cout << e_2 << endl;
			}

			e_2 = e_2 / j;
			if(e_2 < e_3){
				e_3 = e_2;
				d = dp;
			}
		}
		
	//	double durchmesser = Bisektionsverfahren(laenge, normierte_450, normierte_650, T);
		cout << "Durchmesser ist " << d <<endl;
		
		cout << "Die Abkühlkurve ist: " << endl;
		double** Abkuehlkurve = runge(zeitschritt, laenge, d, T);
		/*for(int i = 0; i< laenge; i++){
			for(int j = 0; j < 2; j++){
				cout << Abkuehlkurve[i][j] <<"   ";
			}
			cout << endl;
		}*/
		
		cout << "Das Signal 450nm ist: " << endl;
		double** Signal_450 = new double* [laenge]; //Erstellen der wahren Signalkurven
		Signal_450 = StrahlungskurveBerechnen (laenge, Abkuehlkurve, lambda1, d);
		/*for(int i = 0; i < laenge; i++){
			for(int j = 0; j < 2; j++){
				cout << Signal_450[i][j] <<"   ";
			}
			cout << endl;
		}*/
		cout << "Das Signal 650nm ist: " << endl;		
		double** Signal_650 = new double* [laenge];
		Signal_650 = StrahlungskurveBerechnen (laenge, Abkuehlkurve, lambda2, d);
    	
		loeschen(eBP_450,z_450);
    	loeschen(eBP_650,z_650);
    	loeschen(syn_450,z_450 - zmax_450);
    	loeschen(syn_650,z_650 - zmax_650);
	}
	if(Messreihe == 2){
    	z_450 = groesse("C:\\BP2_450nm.txt");
    	double** eBP_450 = lesen("C:\\BP2_450nm.txt", z_450); 

    	z_650 = groesse("C:\\BP2_650nm.txt");
    	double** eBP_650 = lesen("C:\\BP2_650nm.txt", z_650);
    	
    	zmax_450 = maxsignalz(z_450,eBP_450); //Zeile beim Startwert
    	zmax_650 = maxsignalz(z_650,eBP_650);
		
		smax_450 = eBP_450[zmax_450][1]; //maximales Signal (Startwert)
		smax_650 = eBP_650[zmax_650][1];
		int laenge = min(z_450-zmax_450, z_650-zmax_650); //Ermitteln der Länge des Arrays
    	
    	double** syn_450 = new double* [laenge]; //synchronisieren
    	syn_450 = synchronisation(zmax_450, z_450, eBP_450);
    	double** syn_650 = new double* [laenge];
    	syn_650 = synchronisation(zmax_650, z_650, eBP_650);
    	
		T = Tmax(smax_450, smax_650);
    	cout << "Die maximale Temperatur ist " << T << endl;
		
		double zeitschritt = syn_450[1][0] - syn_450[0][0]; // Berechnen des Zeitschrittes für Runge Kutta Verfahren

		//Normierung der eingelesenen Messdaten
		double** normierte_450 = new double* [laenge];
		normierte_450 = normierung (syn_450, laenge);
		double** normierte_650 = normierung(syn_650, laenge);
		normierte_650 = normierung(syn_650, laenge);
				
		long double d = 0;
		long double dmin = 1e-8;
		long double dmax = 1e-6;
		long double e_2 = 0;
		long double e_3 = 1e20;
		long double j = 0;
		for(long double dp = dmin;dp <= dmax; dp += 2e-9){	
			double** vergleichsmatrix_450 = new double* [laenge];
			vergleichsmatrix_450 = berechneSignalkurve(dp,T,laenge,1e-8,lambda1);
			double** vergleichsmatrix_650 = new double* [laenge];
			vergleichsmatrix_650 = berechneSignalkurve(dp,T,laenge,1e-8,lambda2);
			for (int i = 0; i < laenge; i++) {
				e_2 += (vergleichsmatrix_450[i][1] - normierte_450[i][1]) * (vergleichsmatrix_450[i][1] - normierte_450[i][1]);
				j++;
				//cout << e_2 << endl;
			}
			for (int i = 0; i < laenge; i++) {
				e_2 += (vergleichsmatrix_650[i][1] - normierte_650[i][1]) * (vergleichsmatrix_650[i][1] - normierte_650[i][1]);
				j++;
				//cout << e_2 << endl;
			}

			e_2 = e_2 / j;
			if(e_2 < e_3){
				e_3 = e_2;
				d = dp;
			}	
		}
		
	//	double durchmesser = Bisektionsverfahren(laenge, normierte_450, normierte_650, T);
		cout<<"Durchmesser ist "<< d <<endl;
		
		cout << "Die Abkühlkurve ist: " << endl;
		double** Abkuehlkurve = runge(zeitschritt, laenge, d, T);
		/*for(int i = 0; i< laenge; i++){
			cout << Abkuehlkurve[i][1];
			cout << endl;
		}*/
		
		cout << "Das Signal 450nm ist: " << endl;
		double** Signal_450 = new double* [laenge]; //Erstellen der wahren Signalkurven
		Signal_450 = StrahlungskurveBerechnen (laenge, Abkuehlkurve, lambda1, d);
		cout << "Das Signal 650nm ist: " << endl;		
		double** Signal_650 = new double* [laenge];
		Signal_650 = StrahlungskurveBerechnen (laenge, Abkuehlkurve, lambda2, d);
		double ** normierte_Signal = normierung(Signal_650, laenge);
		for(int i = 0; i< laenge; i++){
			cout << normierte_Signal[i][1];
			cout << endl;
		}
    	loeschen(eBP_450,z_450);
    	loeschen(eBP_650,z_650);
    	loeschen(syn_450,z_450 - zmax_450);
    	loeschen(syn_650,z_650 - zmax_650);
	}
    if(Messreihe == 3){
		z_450 = groesse("C:\\BP3_450nm.txt");
    	double** eBP_450 = lesen("C:\\BP3_450nm.txt", z_450); 

    	z_650 = groesse("C:\\BP3_650nm.txt");
    	double** eBP_650 = lesen("C:\\BP3_650nm.txt", z_650);
    	
    	zmax_450 = maxsignalz(z_450,eBP_450); //Zeile beim Startwert
    	zmax_650 = maxsignalz(z_650,eBP_650);
		
		smax_450 = eBP_450[zmax_450][1]; //maximales Signal (Startwert)
		smax_650 = eBP_650[zmax_650][1];
		int laenge = min(z_450-zmax_450, z_650-zmax_650); //Ermitteln der Länge des Arrays
    	
    	double** syn_450 = new double* [laenge]; //synchronisieren
    	syn_450 = synchronisation(zmax_450, z_450, eBP_450);
    	double** syn_650 = new double* [laenge];
    	syn_650 = synchronisation(zmax_650, z_650, eBP_650);
    	
		T = Tmax(smax_450, smax_650);
    	cout << "Die maximale Temperatur ist " << T << endl;
		
		double dp = 1e-9; //ertster Partikeldurchmesser
		double zeitschritt = syn_450[1][0] - syn_450[0][0]; // Berechnen des Zeitschrittes für Runge Kutta Verfahren

		//Normierung der eingelesenen Messdaten
		double** normierte_450 = new double* [laenge];
		normierte_450 = normierung (syn_450, laenge);
		double** normierte_650 = normierung(syn_650, laenge);
		normierte_650 = normierung(syn_650, laenge);
		
		long double d = 0;
		long double dmin = 1e-8;
		long double dmax = 1e-6;
		long double e_2 = 0;
		long double e_3 = 1e20;
		long double j = 0;
		for(long double dp = dmin;dp <= dmax; dp += 2e-9){	
			double** vergleichsmatrix_450=new double* [laenge];
			vergleichsmatrix_450=berechneSignalkurve(dp,T,laenge,1e-8,lambda1);
			double** vergleichsmatrix_650=new double* [laenge];
			vergleichsmatrix_650=berechneSignalkurve(dp,T,laenge,1e-8,lambda2);
			for (int i = 0; i < laenge; i++) {
				e_2 += (vergleichsmatrix_450[i][1] - normierte_450[i][1]) * (vergleichsmatrix_450[i][1] - normierte_450[i][1]);
				j++;
				//cout << e_2 << endl;
			}
			for (int i = 0; i < laenge; i++) {
				e_2 += (vergleichsmatrix_650[i][1] - normierte_650[i][1]) * (vergleichsmatrix_650[i][1] - normierte_650[i][1]);
				j++;
				//cout << e_2 << endl;
			}

			e_2 = e_2 / j;
			if(e_2<e_3){
				e_3=e_2;
				d=dp;
			}
		}
		
	//	double durchmesser = Bisektionsverfahren(laenge, normierte_450, normierte_650, T);
		cout<<"Durchmesser ist "<< d <<endl;
		
		cout << "Die Abkühlkurve ist: " << endl;
		double** Abkuehlkurve = runge(zeitschritt, laenge, d, T);
		
		cout << "Das Signal 450nm ist: " << endl;
		double** Signal_450 = new double* [laenge]; //Erstellen der wahren Signalkurven
		Signal_450 = StrahlungskurveBerechnen (laenge, Abkuehlkurve, lambda1, d);
		/*for(int i = 0; i < laenge; i++){
			for(int j = 0; j < 2; j++){
				cout << Signal_450[i][j] <<"   ";
			}
			cout << endl;
		}*/
		cout << "Das Signal 650nm ist: " << endl;		
		double** Signal_650 = new double* [laenge];
		Signal_650 = StrahlungskurveBerechnen (laenge, Abkuehlkurve, lambda2, d);
		/*double ** normierte_Signal = normierung(Signal_650, laenge);
		cout << "normierte: " << endl;
		for(int i = 0; i < laenge; i++){
			cout << normierte_Signal[i][1];
			cout << endl;
		}*/
    	loeschen(eBP_450,z_450);
    	loeschen(eBP_650,z_650);
    	loeschen(syn_450,z_450 - zmax_450);
    	loeschen(syn_650,z_650 - zmax_650);
	}
	return 0;
}
