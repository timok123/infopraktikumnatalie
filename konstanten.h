#pragma once
#include <complex>

const double lambda1 = 450e-9; //Wellenlänge 450 m
const double lambda2 = 650e-9; //Wellenlänge 650 m
const double h = 6.62607e-34; // Plancksches Wirkungsquantum in m²*kg/ s
const double c0 = 299792458; //Lichtgeschwindigkeit in m/s
const double b = 1.3806e-23; //Boltzmann-Konstante in m²*kg/ s²*K
const double n = 1e14; // Partikelanzahlkonzentration in 1/m³
const double R = 0.1; //Abstand zum Detektor in m
const std::complex<double> m(3.28,-4.52); //Komplexer Brechungsindex
const double Tg = 293.15; //Gastetmperatur in K


const double k_g = 0.1068; //Wärmeleitfähigkeit Stickstoff
const double k = 1.4; // Isentropenexponent
const double alpha_T = 0.3; //Akkommodationskoeffizient
const double f = (9.0 * k - 5.0) / 4.0;
const double G = 8.0 * f / (alpha_T * (k - 1));
const double lambda_mg = 67e-9; //Mittlere freie Wellenlänge in m

const double roh = 7870.0; //Partikeldichte in kg/m³
const double c_p_Fe = 824.0; //spez. Wärmekapazität d. Partikel in kg/ K